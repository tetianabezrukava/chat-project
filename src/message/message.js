import React from 'react';
import { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHeart } from '@fortawesome/free-solid-svg-icons';

import './messege.css'

export default class Message extends Component {
  constructor(props) {
    super(props);
    this.state = {
      like: false
    };
  }
  
  likeClickHandle = () => {
    this.setState((state) => {
      return {like: !state.like }
    })
  }
  
  render() {
    const { userAvatar, userName, text, time } = this.props.messageData;
    const  likeClasses = this.state.like ? 'heart-icon message-liked' : 'heart-icon message-like';
    return (
      <div className="message">
        <img className="message-user-avatar" src={userAvatar} alt="avatar" />
        <div className="message-user-name">{userName}</div>
        <div className="message-text">{text}</div>
        <div className="message-time">{time}</div>
        <FontAwesomeIcon
          onClick={this.likeClickHandle}
          className={likeClasses}
          icon={faHeart} />
      </div>
    )
  }
}
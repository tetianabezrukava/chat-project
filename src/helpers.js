import moment from 'moment';

const getUniqUsers = (arr) => {
  const userIds = arr.map((item) => item.userId);
  const uniqUserIds = new Set(userIds);
  return uniqUserIds.size
}

const getMessagesCount = (arr) => {
  return arr.length
}

const getLastMessageTime = (arr) => {
  const messagesSortedByTime = [...arr].sort((elemA, elemB) => elemB - elemA);
  if (!messagesSortedByTime.length) {
    return null
  } else {
    const lastMessageTime = messagesSortedByTime[messagesSortedByTime.length - 1].createdAt;
    return moment(lastMessageTime).format('DD.MM.YYYY HH:mm')
  }
}

export function getDataForHeader (arr, title) {
  return {
    title,
    usersCount: getUniqUsers(arr),
    messagesCount: getMessagesCount(arr),
    lastMessageDate: getLastMessageTime(arr),
  }
}
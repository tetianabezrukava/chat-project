import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt, faTrash } from '@fortawesome/free-solid-svg-icons';

import './ownMessage.css'

export default  function OwnMessage({ messageData }) {
  const {id, text, time, handleDelete, handleEdit} = messageData;
  return (
    <div className="own-message">
      <div className="message-text">{text}</div>
      <div className="message-time">{time}</div>
      <FontAwesomeIcon
        className="message-edit icon edit-icon"
        onClick={() => handleEdit(id, text)}
        icon={faPencilAlt}/>
      <FontAwesomeIcon
        className="message-delete icon delete-icon"
        onClick={() => handleDelete(id)}
        icon={faTrash}/>
    </div>
  )
}

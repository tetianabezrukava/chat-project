import React from 'react';
import moment from 'moment';
import { v4 as uuidv4 } from 'uuid';

import Message from '../message';
import OwnMessage from '../ownMessage';

import './messageList.css';

export default function MessageList ({ messages, currentUserId, handleDelete, handleEdit }) {
  const messageDates = messages.map((item) => moment(item.createdAt).format("dddd, DD MMMM YYYY"));
  const uniqDates = [...new Set(messageDates).values()] ;
  
  const content = uniqDates.map((data) => {
    let timeMark;
    switch (data) {
      case moment().format("dddd, DD MMMM YYYY"):
        timeMark = 'Today';
        break;
      case moment().subtract(1, 'days').format("dddd, DD MMMM YYYY"):
        timeMark = 'Yesterday';
        break;
      default:
        timeMark = data.slice(0, data.length - 5);
    }
    
    const content = (
      [...messages]
        .sort((itemA, itemB) => itemB - itemA)
        .filter(item => moment(item.createdAt).format("dddd, DD MMMM YYYY") === data)
        .map((item) => {
          const {id, avatar, user, userId, text, createdAt} = item;
          const isCurrentUser = currentUserId === userId;
          let messageData;
          if (isCurrentUser) {
            messageData = {
              id,
              handleDelete,
              handleEdit,
              text: text,
              time: moment(createdAt).format('HH:mm')
            }
          } else {
            messageData = {
              userAvatar: avatar,
              userName: user,
              text: text,
              time: moment(createdAt).format('HH:mm')
            }
          }
          
          return (
            <li key={id}>
              {isCurrentUser ? <OwnMessage messageData={messageData} /> : <Message messageData={messageData} />}
            </li>
          )
        })
    )
    
    return (
      <li key={uuidv4()}>
        <span className="messages-divider">{timeMark}</span>
        <ul>
          {content}
        </ul>
      </li>
    )
  })
  
  return (
    <div className="message-list">
      <ul>
        {content}
      </ul>
      {/*<div id="list-anchor" />*/}
    </div>
  )
}
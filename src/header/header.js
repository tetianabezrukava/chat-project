import React from 'react';
import './header.css'

export default function Header({ headerData }) {
  const { title, usersCount, messagesCount, lastMessageDate } = headerData;
  return (
    <div className='header'>
      <div className='header-title'>{title}</div>
      <div><span className='header-users-count'>{usersCount}</span> participants</div>
      <div><span className='header-messages-count'>{messagesCount}</span> messages</div>
      <div>last message at <span className='header-last-message-date'>{lastMessageDate}</span></div>
    </div>
  )
}
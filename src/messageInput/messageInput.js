import React from 'react';

import './messageInput.css';

export default function MessageInput({ inputValue, handleSubmit, handleChange }) {
  return (
    <form
      className="message-input"
      onSubmit={handleSubmit}
    >
      <input
        className="message-input-text"
        type="text"
        value={inputValue}
        onChange={handleChange}
      />
      <button
        className="message-input-button"
        type="submit"
      >
        Send
      </button>
    </form>
  )
}
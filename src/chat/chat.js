import React from 'react';
import { Component, Fragment } from 'react';
import { v4 as uuidv4 } from 'uuid';
import moment from 'moment';
import Services from '../services';
import { getDataForHeader } from '../helpers';

import Preloader from '../preloader';
import Header from '../header';
import MessageList from '../messageList';
import MessageInput from '../messageInput';

import './chat.css';

export default class Chat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      messages: null,
      inputValue: '',
      editMessageId: ''
    };
  }
  
  userName = 'John';
  userId = uuidv4();
  userAvatar = 'https://padfieldstout.com/wp-content/uploads/2020/01/John_Easter_Padfield_Stout.jpg'
  
  service = new Services();
  
  componentDidMount() {
    this.service.getData(this.props.url)
    .then((data) => this.setState({
      loading: false,
      messages: data
    }));
  }
  
  handleChange = (ev) => {
    this.setState({
      inputValue: ev.target.value
    })
  }
  
  handleSubmit = (ev) => {
    ev.preventDefault();
    const { editMessageId, inputValue } = this.state;
    
    if (editMessageId) {
      const { messages } = this.state
      const editMessage = messages.find(item => item.id === editMessageId);
      if (editMessage.text ===  inputValue) {
        this.setState({
          inputValue: '',
          editMessage: ''
        })
      } else {
        editMessage.text = inputValue;
        const editMessageIdx = messages.findIndex(item => item === editMessage);
        const newMessages = [
          ...messages.slice(0, editMessageIdx),
          {...editMessage, text: inputValue, editedAt: moment().format()},
          ...messages.slice(editMessageIdx + 1)
        ];
  
        this.setState({
          messages: newMessages,
          inputValue: '',
          editMessage: ''
        })
      }
    } else {
      const newMessage = {
        id: uuidv4(),
        userId: this.userId,
        avatar: this.userAvatar,
        user: this.userName,
        text: this.state.inputValue,
        createdAt: moment().format(),
        editedAt: ''
      }
  
      this.setState((state) => {
        return (
          {
            messages: [...state.messages, newMessage],
            inputValue: ''
          }
        )
      })
    }
    
    const block = document.querySelector('.message-list');
    block.scrollTop = block.scrollHeight;
  }
  
  handleDelete = (id) => {
    const { messages } = this.state
    const deleteMessage = messages.find(item => item.id === id);
    const deleteMessageIdx = messages.findIndex(item => item === deleteMessage);
    const newMessages = [
      ...messages.slice(0, deleteMessageIdx),
      ...messages.slice(deleteMessageIdx + 1)
    ];
    this.setState({
      messages: newMessages
    })
  }
  
  handleEdit = (id, text) => {
    this.setState({
      inputValue: text,
      editMessageId: id
    })
  }
  
  render() {
    let headerData;
    if (this.state.messages) {
      headerData = getDataForHeader(this.state.messages, "Work Chat")
    }
    
    const content = (
      <Fragment>
        <Header headerData={headerData} />
        <MessageList
          currentUserId={this.userId}
          messages={this.state.messages}
          handleEdit={this.handleEdit}
          handleDelete={this.handleDelete}
        />
        <MessageInput
          handleChange={this.handleChange}
          handleSubmit={this.handleSubmit}
          inputValue={this.state.inputValue}
        />
      </Fragment>
    )
    return (
      <div className='chat'>
        { this.state.loading ? <Preloader /> :  content }
      </div>
    )
  }
}
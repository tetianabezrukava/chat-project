import React from 'react';
import './preloader.css'

export default function Preloader() {
  return (
    <div className="loadingio-spinner-spinner-kikza5a3zsp preloader">
      <div className="ldio-24hjhnkzb1q">
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
      </div>
    </div>
  )
  
}